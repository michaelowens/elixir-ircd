defmodule IRCd.ProtocolTest do
  use ExUnit.Case, async: true
  doctest IRCd.Protocol

  test "should parse strings without linefeeds" do
    assert IRCd.Protocol.parse_input("CAP LS") == {:ok, "CAP", ["LS"]}
    assert IRCd.Protocol.parse_input("USER xikeon 0 * :Michael") == {:ok, "USER", ["xikeon", "0", "*", "Michael"]}
  end

  test "should parse strings with linefeeds" do
    assert IRCd.Protocol.parse_input("CAP LS\r\n") == {:ok, "CAP", ["LS"]}
    assert IRCd.Protocol.parse_input("USER xikeon 0 * :Michael\r\n") == {:ok, "USER", ["xikeon", "0", "*", "Michael"]}
  end

  test "should parse strings with last parameter with spaces (:)" do
    assert IRCd.Protocol.parse_input("CAP LS") == {:ok, "CAP", ["LS"]}
    assert IRCd.Protocol.parse_input("USER xikeon 0 * :Michael Owens") == {:ok, "USER", ["xikeon", "0", "*", "Michael Owens"]}
  end
end
