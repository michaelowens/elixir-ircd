defmodule IRCd.Protocol do
  require Logger

  def rpl(key) do
    case key do
      :welcome -> "001"
      :yourhost -> "002"
      :created -> "003"
      :myinfo -> "004"
      :bounce -> "005"
      _ -> "000"
    end
  end

  def parse_input(data) do
    string = to_string(data) |> String.strip

    # Extract command
    [command | rest] = String.split(string, " ", global: false)
    string = Enum.join(rest, " ")

    # Extract final parameter
    [string | final] = String.split(string, ":", global: false)

    # Split arguments
    args = string |> String.strip |> String.split(" ", global: false)

    # Add final parameter if found
    if final do
      args = args ++ final
    end

    {:ok, command, args}
  end

  def format_numeric(sender, num, target, string) when is_atom(num) do
    format_numeric(sender, rpl(num), target, string)
  end

  def format_numeric(sender, num, target, string) do
    unless String.contains?(string, ":"), do: string = ":#{string}"
    ":#{sender} #{num} #{target} #{string}"
  end
end
