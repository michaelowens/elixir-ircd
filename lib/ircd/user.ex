defmodule IRCd.User do
  use GenServer
  use Timex

  require Logger
  require Record

  Record.defrecord :user, [uuid: nil, connected_at: nil, host: nil, ip: nil,
                          socket: nil, nick: nil, registered: false,
                          registered_at: nil, username: nil, hostname: nil,
                          servername: nil, realname: nil]

  ## Client API

  def start_link(ref), do: :gen_server.start_link(__MODULE__, :ok, name: ref)


  ## Server Callbacks

  def init(:ok) do
    state = user(uuid: UUID.uuid1(), connected_at: DateTime.now)
    Logger.debug "New user connected: #{user(state, :uuid)}!"
    {:ok, state}
  end

  def get_mask(state) do
    if user(state, :registered) do
      "#{user(state, :nick)}!#{user(state, :username)}@#{user(state, :host)}"
    else
      "anonymous!unregistered@server"
    end
  end

  def handle_call({:get_mask}, _from, state) do
    {:reply, get_mask(state), state}
  end

  def handle_cast({:create, socket}, state) do
    {:ok, {ip, _port}} = :inet.peername(socket)
    ip_string = to_string(:inet_parse.ntoa(ip))
    host = case :inet.gethostbyaddr(ip) do
      {:ok, {:hostent, hostname, _, _, _, _}} -> to_string(hostname)
      {:error, _} -> to_string(ip_string)
    end
    state = user(state, host: host, ip: ip_string, socket: socket) #.ip(ip_string).socket(socket)
    {:noreply, state}
  end

  def handle_cast({:shutdown}, state) do
    socket = user(state, :socket)
    if socket do
      :gen_tcp.close(socket)
      state = user(state, socket: nil)
    end
    Logger.info "[#{user(state, :uuid)}] Disconnected"
    {:stop, :normal, state}
  end

  def handle_info({:tcp, socket, data}, state) do
    Logger.debug "<<< #{inspect data}"

    {:ok, command, args} = IRCd.Protocol.parse_input(data)
    command = command |> String.downcase |> String.to_atom

    state = case handle_irc(command, args, state) do
      {:ok, state} -> state
      {:ok, reply, state} ->
        write(socket, reply)
        state
    end

    {:noreply, state}
  end

  def handle_info({:tcp_closed, _pid}, state) do
    Logger.debug "tcp_closed from #{user(state, :uuid)}"
    handle_irc(:quit, ["Connection Closed"], state)
    {:noreply, state}
  end

  def handle_info(info, state) do
    Logger.debug "Uncaught info: #{inspect info}"
    {:noreply, state}
  end

  def handle_irc(:quit, [], state), do: handle_irc(:quit, ["Client Quit"], state)
  def handle_irc(:quit, [reason], state) do
    :gen_server.cast(MyServer, {:quit, user(state, :uuid), reason})
    {:ok, state}
  end

  ### Ping pong
  def handle_irc(:ping, [host], state) do
    if host != IRCd.name do
      {:ok, snumeric("402", "No such server", state), state}
    else
      {:ok, "PONG #{host}", state}
    end
  end
  def handle_irc(:ping, [], state), do: {:ok, snumeric("409", "No origin specified", state), state}

  ### Capabilities
  def handle_irc(:cap, ["LS"], state) do
    {:ok, "CAP * LS :", state}
  end

  def handle_irc(:cap, ["END"], state) do
    {:ok, state}
  end

  ### Nick
  def handle_irc(:nick, [nick], state) do
    Logger.debug "Received nick: #{nick}"

    case call_server(:nick, [nick], state) do
      :fail -> {:ok, ":#{IRCd.name} 443 #{nick} :Nickname already in use.", state}
      :ok ->
        state = user(state, nick: nick)
        {replies, state} = send_register(state)
        {:ok, replies, state}
    end
  end

  ### User
  def handle_irc(:user, [username, hostname, servername, realname], state) do
    case user(state, :registered) do
      true -> {:ok, state}
      _ ->
        state = user(state, username: username, hostname: hostname,
                            servername: servername, realname: realname)
        {replies, state} = send_register(state)
        {:ok, replies, state}
    end
  end

  ### Join
  def handle_irc(:join, [channel], state) do
    case call_server(:join, [user(state, :nick), channel], state) do
      :fail -> {:ok, snotice("Could not join channel #{channel}", state), state}
      :ok ->
        replies = [
          ":#{get_mask(state)} JOIN #{channel}",
          snumeric("332", "#{channel} :Some fake topic", state)
        ]
        {:ok, replies, state}
    end
  end

  ### Uncaught commands
  def handle_irc(command, args, state) do
    Logger.debug "Uncaught command: #{command} args: #{inspect args}"
    {:ok, state}
  end


  defp snumeric(num, string, state) do
    IRCd.Protocol.format_numeric(IRCd.name, num, user(state, :nick), string)
  end

  defp snotice(notice, state), do: "NOTICE #{user(state, :nick)} :#{notice}"

  defp send_register(state) do
    if user(state, :registered) do
      Logger.info "User already registered"
      {[], snumeric("462", "You may not reregister", state), state}
    else
      if user(state, :nick) != nil and user(state, :username) != nil and user(state, :realname) != nil do
        Logger.info "Registered user #{inspect state}"
        state = user(state, registered: true, registered_at: DateTime.now)
        replies = [
          snumeric(:welcome, "Welcome to #{IRCd.network_name}, #{user(state, :nick)}", state),
          snumeric(:yourhost, "Your host is #{IRCd.name}, running #{IRCd.full_name}", state),
          snumeric(:created, "This server was created Mon Aug 4 2042 at 00:04:20", state),
          snumeric(:myinfo, "#{IRCd.name} #{IRCd.full_name} DQRSZaghilopswz CFILPQSbcefgijklmnopqrstvz bkloveqjfI", state),
          snumeric(:bounce, "CHANTYPES=# PREFIX=(ov)@+ NETWORK=#{IRCd.network_name} :are supported by this server", state),
          snumeric("251", "There are 420 users and 420 invisible on 420 servers", state),
          snumeric("254", "420 :channels formed", state),
          snumeric("255", ":I have 420 clients and 420 servers", state),
          snumeric("265", "6 8 :Current local users 6, max 8", state),
          snumeric("266", "6 8 :Current global users 6, max 8", state),
          snumeric("250", ":Highest connection count: 420 (420 clients) (11 connections received)", state),
          snotice("Your connection identifier is #{inspect user(state, :uuid)}", state)
        ]
        # {[replies | motd], state}
        {replies, state}
      else
        Logger.debug "Register sequence not complete"
        {[], state}
      end
    end
  end


  ## Private

  # Call/cast to server
  defp call_server(action, args, state) do
    :gen_server.call(MyServer, {action, self, user(state, :uuid), args})
  end
  defp cast_server(action, args, state) do
    :gen_server.cast(MyServer, {action, self, user(state, :uuid), args})
  end

  # Write data to socket
  defp write(socket, data) when is_list(data) do
    write(socket, Enum.join(List.flatten(data), "\r\n"))
  end
  defp write(socket, data) when is_binary(data) do
    Logger.debug ">>> #{data}"
    unless data == "" do
      :gen_tcp.send(socket, data <> "\r\n")
    end
  end
end
