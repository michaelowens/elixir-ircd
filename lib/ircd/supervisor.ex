defmodule IRCd.Supervisor do
  use Supervisor

  require Logger

  def start_link do
    Supervisor.start_link(__MODULE__, :ok, [])
  end

  def init(:ok) do
    Logger.info "Starting supervisor"
    children = [
      supervisor(IRCd.Server, [])
    ]
    supervise(children, strategy: :one_for_one)
  end
end
