defmodule IRCd.Server do
  use GenServer

  require Logger
  require Record

  Record.defrecord :server, users: [], channels: []

  ## Client API

  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: MyServer)
  end

  ## Server API

  def init(:ok) do
    Logger.info "Setting up server"
    case :gen_tcp.listen(IRCd.port, [packet: :line, reuseaddr: true]) do
      {:ok, socket} ->
        Logger.info "Accepting connections on port #{IRCd.port}"
        spawn(IRCd.Server, :accept_connection, [socket])
        {:ok, server()}

      {:error, reason} ->
        Logger.error "Error listening on port #{IRCd.port}: #{reason}"
        {:stop, "Could not listen on port #{IRCd.port}: #{reason}"}
    end
  end

  def accept_connection(socket) do
    case :gen_tcp.accept(socket) do
      {:ok, client} ->
        {:ok, pid} = IRCd.User.start_link(:erlang.make_ref())
        :ok = :gen_tcp.controlling_process(client, pid)
        :gen_server.cast(pid, {:create, client})

      {:error, reason} ->
        Logger.error "Could not accept client: #{inspect reason}"
        exit(reason)
    end

    accept_connection(socket)
  end

  def handle_call({:nick, pid, uuid, [new_nick]}, _from, state) do
    Logger.debug "got nick #{new_nick}"
    Logger.debug "before: #{inspect server(state, :users)}"

    case List.keyfind(server(state, :users), new_nick, 2) do
      {_, _, _, _} ->
        state = server(state, users: [{pid, uuid, new_nick, []} | server(state, :users)])
        {:reply, :fail, state}
      _ ->
        state = server(state, users: [{pid, uuid, new_nick, []} | server(state, :users)])
        Logger.debug "after: #{inspect server(state, :users)}"
        {:reply, :ok, state}
    end
  end

  def handle_call({:join, _pid, _uuid, [nick, channel]}, _from, state) do
    {:reply, :ok, state}
  end

  def handle_cast({:quit, id, reason}, state) do
    Logger.debug "Got :quit"
    {state, u_pid} = process_quit(id, reason, state)
    Logger.debug "quitting #{inspect u_pid}"
    :gen_server.cast(u_pid, {:shutdown})
    {:noreply, state}
  end

  def process_quit(id, reason, state) do
    Logger.debug "search for u_pid for: #{id}"
    case List.keyfind(server(state, :users), id, 1) do
      {u_pid, _, _, _} ->
        Logger.debug "found u_pid: #{inspect u_pid}"
        state = server(state, users: List.keydelete(server(state, :users), id, 1))
        {state, u_pid}

      _ -> {state, 0}
    end
  end
end
