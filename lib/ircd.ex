defmodule IRCd do
  use Application

  require Logger

  def name, do: "xikeon.irc"
  def network_name, do: "xikIRC"
  def version, do: "0.0.1"
  def full_name, do: "#{name}-#{version}"
  def port, do: 6667

  def start(_type, _) do
    Logger.info "Starting daemon"
    IRCd.Supervisor.start_link
  end

  def stop(_state) do
    Logger.info "Stopping daemon"
  end
end
